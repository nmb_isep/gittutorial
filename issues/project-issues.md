# Project Issues

The issues to be performed.

## Issues For Step 2

### Instructor

| Issue # | 1 |
| --- | --- |
| Name | Create Animal class |
| Description | Assign to Instructor only |
| Commit message | Class Animal Added

```java 
public class Animal {
    public Animal() { 
    }
}
```

### Team

| Issue # | 2 |
| --- | --- |
| Name | Create Bird class |
| Description | Create Bird class |
| Commit message | Class Bird Added |

```java
public class Bird{
    public Bird() { 
    }
}

```

| Issue # | 3 |
| --- | --- |
| Name | Create Cat class |
| Description | Create Cat class |
| Commit message | Class Cat Added |

```java
public class Cat{
    public Cat() { 
    }
}
```

| Issue # | 4 |
| --- | --- |
| Name | Create Cow class |
| Description | Create Cow class |
| Commit message | Class Cow Added |

```java
public class Cow{
    public Cow() { 
    }
}

```

| Issue # | 5 |
| --- | --- |
| Name | Create Cricket class |
| Description | Create Cricket class |
| Commit message | Class Cricket Added |

```java
public class Cricket{
    public Cricket() { 
    }
}

```

| Issue # | 6 |
| --- | --- |
| Name | Create Dog class |
| Description | Create Dog class |
| Commit message | Class Dog Added |

```java
public class Dog{
    public Dog() { 
    }
}
```

| Issue # | 7 |
| --- | --- |
| Name | Create Donkey class |
| Description | Create Donkey class |
| Commit message | Class Donkey Added |

```java
public class Donkey{
    public Donkey() { 
    }
}
```

| Issue # | 8 |
| --- | --- |
| Name | Create Duck class |
| Description | Create Duck class |
| Commit message | Class Duck Added |

```java
public class Duck {
    public Duck() { 
    }
}
```

| Issue # | 9 |
| --- | --- |
| Name | Create Pig class |
| Description | Create Pig class |
| Commit message | Class Pig Added |

```java
public class Pig{
    public Pig () { 
    }
}
```

| Issue # | 10 |
| --- | --- |
| Name | Create Rooster class |
| Description | Create Rooster class |
| Commit message | Class Rooster Added |

```java
public class Rooster{
    public Rooster() { 
    }
}
```

| Issue # | 11 |
| --- | --- |
| Name | Create Sheep class |
| Description | Create Sheep class |
| Commit message | Class Sheep Added |

```java
public class Sheep{
    public Sheep() { 
    }
}
```

| Issue # | 12 |
| --- | --- |
| Name | Create Turkey class |
| Description | Create Turkey class |
| Commit message | Class Turkey Added |

```java
public class Turkey{
    public Turkey() { 
    }
}
```

## Issues For Step 3

### Team

| Issue # | 13 |
| --- | --- |
| Name | Add sound to Bird |
| Description | Add the following method to Bird class. |
| Commit message | Added sound behaviour to Bird. |

```java 
    void makeSound() { 
        System.out.println("piu piu");
    }
```

| Issue # | 14 |
| --- | --- |
| Name | Add sound to Cat |
| Description | Add the following method to class Cat. |
| Commit message | Added sound behaviour to Cat. |

```java 
    void makeSound() {
        System.out.println("miau");
    }
```

| Issue # | 15 |
| --- | --- |
| Name | Add sound to Cow |
| Description | Add the following method to class Cow. |
| Commit message | Added sound behaviour to Cow. |

```java 
    void makeSound() {
        System.out.println("muuuuu");
    }
```

| Issue # | 16 |
| --- | --- |
| Name | Add sound to Cricket |
| Description | Add the following method to class Cricket. |
| Commit message | Added sound behaviour to Cricket. |

```java 
    void makeSound() {
        System.out.println("cri cri");
    }
```

| Issue # | 17 |
| --- | --- |
| Name | Add sound to Dog |
| Description | Add the following method to class Dog. |
| Commit message | Added sound behaviour to Dog. |

```java 
    void makeSound() {
        System.out.println("au au");
    }
```

| Issue # | 18 |
| --- | --- |
| Name | Add sound to Donkey |
| Description | Add the following method to class Donkey. |
| Commit message | Added sound behaviour to Donkey. |

```java 
    void makeSound() {
        System.out.println("ió ió");
    }
```

| Issue # | 19 |
| --- | --- |
| Name | Add sound to Duck |
| Description | Add the following method to class Duck. |
| Commit message | Added sound behaviour to Duck. |

```java 
    void makeSound() {
        System.out.println("qua qua");
    }
```

| Issue # | 20 |
| --- | --- |
| Name | Add sound to Pig |
| Description | Add the following method to class Pig. |
| Commit message | Added sound behaviour to Pig. |

```java 
    void makeSound() {
        System.out.println("oinc oinc");
    }
```

| Issue # | 21 |
| --- | --- |
| Name | Add sound to Rooster |
| Description | Add the following method to class Rooster. |
| Commit message | Added sound behaviour to Rooster. |

```java 
    void makeSound() {
        System.out.println("cócórócócó");
    }
```

| Issue # | 22 |
| --- | --- |
| Name | Add sound to Sheep |
| Description | Add the following method to class Sheep. |
| Commit message | Added sound behaviour to Sheep. |

```java 
    void makeSound() {
        System.out.println("mêêê");
    }
```

| Issue # | 23 |
| --- | --- |
| Name | Add sound to Turkey |
| Description | Add the following method to class Turkey. |
| Commit message | Added sound behaviour to Turkey. |

```java 
    void makeSound() {
        System.out.println("glu glu");
    }
```

## Issues For Step 4

### Team

| Issue # | 24 |
| --- | --- |
| Name | Add fly behaviour to Duck |
| Description | Add fly behaviour to Duck. |
| Commit message | Added fly behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void fly() {  
    }
```

| Issue # | 25 |
| --- | --- |
| Name | Change Duck sound to "qua qua qua" |
| Description | Change the sound Ducks make to "qua qua qua". |
| Commit message | Changed Duck sound to "qua qua qua". Fixes issue #? [replace ? by your issue number] |

```java
    System.out.println("qua qua qua");
```

| Issue # | 26 |
| --- | --- |
| Name | Add quack behaviour to Duck |
| Description | Add quack behaviour to Duck. |
| Commit message | Added quack behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void quack() {
    }
```

| Issue # | 27 |
| --- | --- |
| Name | Add swim behaviour to Duck |
| Description | Add swim behaviour to Duck. |
| Commit message | Added swim behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void swim() {
    }
```

| Issue # | 28 |
| --- | --- |
| Name | Add lay egg behaviour to Duck |
| Description | Add lay egg behaviour to Duck. |
| Commit message | Added lay egg behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void layEgg() {
        
    }
```

| Issue # | 29 |
| --- | --- |
| Name | Change Duck sound to #qua# |
| Description | Change the sound Ducks make to "qua". |
| Commit message | Changed Duck sound to "qua". Fixes issue #? [replace ? by your issue number] |

```java
    System.out.println("qua");
```

| Issue # | 30 |
| --- | --- |
| Name | Add eat behaviour to Duck |
| Description | Add eat behaviour to Duck. |
| Commit message | Added eat behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void eat() {
    }
```

| Issue # | 31 |
| --- | --- |
| Name | Specify how loud each sound made by a Duck should be |
| Description | Change makeSound method to accept a loudness argument. |
| Commit message | Changed Duck makeSound behaviour to set loudness level. Fixes issue #? [replace ? by your issue number] |

```java 
    void makeSound(int loudness)
```

| Issue # | 32 |
| --- | --- |
| Name | Add sleep behaviour to Duck |
| Description | Add sleep behaviour to Duck. |
| Commit message | Added sleep behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void sleep() {       
    }
```

| Issue # | 33 |
| --- | --- |
| Name | Add blink behaviour to Duck |
| Description | Add blink behaviour to Duck. |
| Commit message | Added blink behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void blink() {        
    }
```

| Issue # | 34 |
| --- | --- |
| Name | Add shake behaviour to Duck |
| Description | Add shake behaviour to Duck. |
| Commit message | Added shake behaviour to Duck. Fixes issue #? [replace ? by your issue number] |

```java 
    void shake() {        
    }
```